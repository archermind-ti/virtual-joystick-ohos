package io.github.controlwear.virtual.joystick;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;


public class JoystickView extends Component implements Component.DrawTask, Component.EstimateSizeListener,
        Component.TouchEventListener, Runnable {

    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "JoystickView");

    /*
    CONSTANTS
    */

    /**
     * Default refresh rate as a time in milliseconds to send move values through callback
     */
    private static final int DEFAULT_LOOP_INTERVAL = 50; // in milliseconds

    /**
     * Used to allow a slight move without cancelling MultipleLongPress
     */
    private static final int MOVE_TOLERANCE = 10;

    /**
     * Default color for button
     */
    private static final int DEFAULT_COLOR_BUTTON = Color.BLACK.getValue();

    /**
     * Default color for border
     */
    private static final int DEFAULT_COLOR_BORDER = Color.TRANSPARENT.getValue();

    /**
     * Default alpha for border
     */
    private static final int DEFAULT_ALPHA_BORDER = -1;//255;

    /**
     * Default background color
     */
    private static final int DEFAULT_BACKGROUND_COLOR = Color.TRANSPARENT.getValue();

    /**
     * Default View's size
     */
    private static final int DEFAULT_SIZE = 200;

    /**
     * Default border's width
     */
    private static final int DEFAULT_WIDTH_BORDER = 3;

    /**
     * Default behavior to fixed center (not auto-defined)
     */
    private static final boolean DEFAULT_FIXED_CENTER = true;


    /**
     * Default behavior to auto re-center button (automatically recenter the button)
     */
    private static final boolean DEFAULT_AUTO_RECENTER_BUTTON = true;


    /**
     * Default behavior to button stickToBorder (button stay on the border)
     */
    private static final boolean DEFAULT_BUTTON_STICK_TO_BORDER = false;

    private Context mContext;

    // DRAWING
    private Paint mPaintCircleButton;
    private Paint mPaintCircleBorder;
    private Paint mPaintBackground;

    private Paint mPaintBitmapButton;
    private PixelMap mButtonBitmap;


    /**
     * Ratio use to define the size of the button
     */
    private float mButtonSizeRatio;


    /**
     * Ratio use to define the size of the background
     *
     */
    private float mBackgroundSizeRatio;


    // COORDINATE
    private int mPosX = 0;
    private int mPosY = 0;
    private int mCenterX = 0;
    private int mCenterY = 0;

    private int mFixedCenterX = 0;
    private int mFixedCenterY = 0;

    /**
     * Used to adapt behavior whether it is auto-defined center (false) or fixed center (true)
     */
    private boolean mFixedCenter;


    /**
     * Used to adapt behavior whether the button is automatically re-centered (true)
     * when released or not (false)
     */
    private boolean mAutoReCenterButton;


    /**
     * Used to adapt behavior whether the button is stick to border (true) or
     * could be anywhere (when false - similar to regular behavior)
     */
    private boolean mButtonStickToBorder;


    /**
     * Used to enabled/disabled the Joystick. When disabled (enabled to false) the joystick button
     * can't move and onMove is not called.
     */
    private boolean mEnabled;


    // SIZE
    private int mButtonRadius;
    private int mBorderRadius;


    /**
     * Alpha of the border (to use when changing color dynamically)
     */
    private int mBorderAlpha;


    /**
     * Based on mBorderRadius but a bit smaller (minus half the stroke size of the border)
     */
    private float mBackgroundRadius;

    /**
     * Listener used to dispatch OnMove event
     */
    private OnMoveListener mCallback;

    private long mLoopInterval = DEFAULT_LOOP_INTERVAL;
    private Thread mThread = new Thread(this);


    /**
     * Listener used to dispatch MultipleLongPress event
     */
    private OnMultipleLongPressListener mOnMultipleLongPressListener;

    private EventRunner eventRunner = EventRunner.create("MyEventRunner");
    private final EventHandler mHandlerMultipleLongPress = new EventHandler(eventRunner);

    private Runnable mRunnableMultipleLongPress;
    private int mMoveTolerance;


    /**
     * Default value.
     * Both direction correspond to horizontal and vertical movement
     */
    private static final int BUTTON_DIRECTION_BOTH = 0;
    private static final int BUTTON_DIRECTION_HORIZONTAL = -1;
    private static final int BUTTON_DIRECTION_VERTICAL = 1;

    /**
     * The allowed direction of the button is define by the value of this parameter:
     * - a negative value for horizontal axe
     * - a positive value for vertical axe
     * - zero for both axes
     */
    private int mButtonDirection = 0;


    /*
    ATTRS STRING
     */

    private static final String JV_BUTTON_COLOR = "JV_buttonColor";
    private static final String JV_BORDER_COLOR = "JV_borderColor";
    private static final String JV_BORDER_ALPHA = "JV_borderAlpha";
    private static final String JV_BACKGROUND_COLOR = "JV_backgroundColor";
    private static final String JV_BORDER_WIDTH = "JV_borderWidth";
    private static final String JV_FIXED_CENTER = "JV_fixedCenter";
    private static final String JV_AUTO_RECENTER_BUTTON = "JV_autoReCenterButton";
    private static final String JV_BUTTON_STICK_BORDER = "JV_buttonStickToBorder";
    private static final String JV_BUTTON_IMAGE = "JV_buttonImage";
    private static final String JV_ENABLED = "JV_enabled";
    private static final String JV_BUTTON_SIZE_RATIO = "JV_buttonSizeRatio";
    private static final String JV_BACKGROUND_SIZE_RATIO = "JV_backgroundSizeRatio";
    private static final String JV_BUTTON_DIRECTION = "JV_buttonDirection";
    private static final String JV_BUTTON_DIRECTION_HORIZONTAL = "horizontal";
    private static final String JV_BUTTON_DIRECTION_BOTH = "both";
    private static final String JV_BUTTON_DIRECTION_VERTICAL = "vertical";

/*
    INTERFACES
    */


    /**
     * Interface definition for a callback to be invoked when a
     * JoystickView's button is moved
     */
    public interface OnMoveListener {

        /**
         * Called when a JoystickView's button has been moved
         * @param angle current angle
         * @param strength current strength
         */
        void onMove(int angle, int strength);
    }


    /**
     * Interface definition for a callback to be invoked when a JoystickView
     * is touched and held by multiple pointers.
     */
    public interface OnMultipleLongPressListener {
        /**
         * Called when a JoystickView has been touch and held enough time by multiple pointers.
         */
        void onMultipleLongPress();
    }

    /*
    CONSTRUCTORS
     */


    /**
     * Simple constructor to use when creating a JoystickView from code.
     * Call another constructor passing null to Attribute.
     * @param context The Context the JoystickView is running in, through which it can
     *        access the current theme, resources, etc.
     */
    public JoystickView(Context context) {
        this(context, null);
    }


    public JoystickView(Context context, AttrSet attrs, int defStyleAttr) {
        this(context, attrs);
    }


    /**
     * Constructor that is called when inflating a JoystickView from XML. This is called
     * when a JoystickView is being constructed from an XML file, supplying attributes
     * that were specified in the XML file.
     * @param context The Context the JoystickView is running in, through which it can
     *        access the current theme, resources, etc.
     * @param attrs The attributes of the XML tag that is inflating the JoystickView.
     */
    public JoystickView(Context context, AttrSet attrs) {
        super(context, attrs);
        HiLog.info(TAG, "init");

        int buttonColor;
        int borderColor;
        int backgroundColor;
        int borderWidth;
        Element buttonElement;

        mContext = context;

        if (attrs.getAttr(JV_BUTTON_COLOR).isPresent() && attrs.getAttr(JV_BUTTON_COLOR).get() != null) {
            buttonColor = attrs.getAttr(JV_BUTTON_COLOR).get().getColorValue().getValue();
        } else {
            buttonColor = DEFAULT_COLOR_BUTTON;
        }

        if (attrs.getAttr(JV_BORDER_COLOR).isPresent() && attrs.getAttr(JV_BORDER_COLOR).get() != null) {
            borderColor = attrs.getAttr(JV_BORDER_COLOR).get().getColorValue().getValue();
        } else {
            borderColor = DEFAULT_COLOR_BORDER;
        }

        if (attrs.getAttr(JV_BORDER_ALPHA).isPresent() && attrs.getAttr(JV_BORDER_ALPHA).get() != null) {
            mBorderAlpha = attrs.getAttr(JV_BORDER_ALPHA).get().getIntegerValue();
        } else {
            mBorderAlpha = DEFAULT_ALPHA_BORDER;
        }

        if (attrs.getAttr(JV_BACKGROUND_COLOR).isPresent() && attrs.getAttr(JV_BACKGROUND_COLOR).get() != null) {
            backgroundColor = attrs.getAttr(JV_BACKGROUND_COLOR).get().getColorValue().getValue();
        } else {
            backgroundColor = DEFAULT_BACKGROUND_COLOR;
        }

        if (attrs.getAttr(JV_BORDER_WIDTH).isPresent() && attrs.getAttr(JV_BORDER_WIDTH).get() != null) {
            borderWidth = attrs.getAttr(JV_BORDER_WIDTH).get().getDimensionValue();
        } else {
            borderWidth = DEFAULT_WIDTH_BORDER;
        }

        if (attrs.getAttr(JV_FIXED_CENTER).isPresent() && attrs.getAttr(JV_FIXED_CENTER).get() != null) {
            mFixedCenter = attrs.getAttr(JV_FIXED_CENTER).get().getBoolValue();
        } else {
            mFixedCenter = DEFAULT_FIXED_CENTER;
        }

        if (attrs.getAttr(JV_AUTO_RECENTER_BUTTON).isPresent() && attrs.getAttr(JV_AUTO_RECENTER_BUTTON).get() != null) {
            mAutoReCenterButton = attrs.getAttr(JV_AUTO_RECENTER_BUTTON).get().getBoolValue();
        } else {
            mAutoReCenterButton = DEFAULT_AUTO_RECENTER_BUTTON;
        }

        if (attrs.getAttr(JV_BUTTON_STICK_BORDER).isPresent() && attrs.getAttr(JV_BUTTON_STICK_BORDER).get() != null) {
            mButtonStickToBorder = attrs.getAttr(JV_BUTTON_STICK_BORDER).get().getBoolValue();
        } else {
            mButtonStickToBorder = DEFAULT_BUTTON_STICK_TO_BORDER;
        }

        if (attrs.getAttr(JV_BUTTON_IMAGE).isPresent() && attrs.getAttr(JV_BUTTON_IMAGE).get() != null) {
            buttonElement = attrs.getAttr(JV_BUTTON_IMAGE).get().getElement();
        } else {
            buttonElement = null;
        }

        if (attrs.getAttr(JV_ENABLED).isPresent() && attrs.getAttr(JV_ENABLED).get() != null) {
            mEnabled = attrs.getAttr(JV_ENABLED).get().getBoolValue();
        } else {
            mEnabled = true;
        }

        if (attrs.getAttr(JV_BUTTON_SIZE_RATIO).isPresent() && attrs.getAttr(JV_BUTTON_SIZE_RATIO).get() != null) {
//            String radio = attrs.getAttr(JV_BUTTON_SIZE_RATIO).get().getStringValue();
//            if (radio.lastIndexOf("%") > 0) {
//                radio = radio.substring(0, radio.lastIndexOf("%"));
//            }
//            mButtonSizeRatio = Float.parseFloat(radio)/100;
            mButtonSizeRatio = getRadioValue(attrs.getAttr(JV_BUTTON_SIZE_RATIO).get().getStringValue());

        } else {
            mButtonSizeRatio = 0.25f;
        }

        if (attrs.getAttr(JV_BACKGROUND_SIZE_RATIO).isPresent() && attrs.getAttr(JV_BACKGROUND_SIZE_RATIO).get() != null) {
//            String radio = attrs.getAttr(JV_BACKGROUND_SIZE_RATIO).get().getStringValue();
//            if (radio.lastIndexOf("%") > 0) {
//                radio = radio.substring(0, radio.lastIndexOf("%"));
//            }
//            mBackgroundSizeRatio = Float.parseFloat(radio)/100;
            mBackgroundSizeRatio = getRadioValue(attrs.getAttr(JV_BACKGROUND_SIZE_RATIO).get().getStringValue());
        } else {
            mBackgroundSizeRatio = 0.75f;
        }

        if (attrs.getAttr(JV_BUTTON_DIRECTION).isPresent() && attrs.getAttr(JV_BUTTON_DIRECTION).get() != null) {
            String direction = attrs.getAttr(JV_BUTTON_DIRECTION).get().getStringValue();
            if (direction.equals(JV_BUTTON_DIRECTION_HORIZONTAL)) {
                mButtonDirection = BUTTON_DIRECTION_HORIZONTAL;
            } else if (direction.equals(JV_BUTTON_DIRECTION_VERTICAL)) {
                mButtonDirection = BUTTON_DIRECTION_VERTICAL;
            }
        }

        // Initialize the drawing according to attributes

        mPaintCircleButton = new Paint();
        mPaintCircleButton.setAntiAlias(true);
        mPaintCircleButton.setColor(new Color(buttonColor));
        mPaintCircleButton.setStyle(Paint.Style.FILL_STYLE);

        if (buttonElement != null) {
            mButtonBitmap = ((PixelMapElement)buttonElement).getPixelMap();
            mPaintBitmapButton = new Paint();
        }

        mPaintCircleBorder = new Paint();
        mPaintCircleBorder.setAntiAlias(true);
        mPaintCircleBorder.setColor(new Color(borderColor));
        mPaintCircleBorder.setStyle(Paint.Style.STROKE_STYLE);
        mPaintCircleBorder.setStrokeWidth(borderWidth);
        HiLog.info(TAG, " init borderColor = "+borderColor);

//        if (borderColor != Color.TRANSPARENT.getValue()) {
//            mPaintCircleBorder.setAlpha(mBorderAlpha);
//        }
        if (mBorderAlpha != DEFAULT_ALPHA_BORDER) {
            mPaintCircleBorder.setAlpha(mBorderAlpha);
        }

        mPaintBackground = new Paint();
        mPaintBackground.setAntiAlias(true);
        mPaintBackground.setColor(new Color(backgroundColor));
        mPaintBackground.setStyle(Paint.Style.FILL_STYLE);
        HiLog.info(TAG, " init backgroundColor = "+backgroundColor);

        // Init Runnable for MultiLongPress

        mRunnableMultipleLongPress = new Runnable() {
            @Override
            public void run() {
                if (mOnMultipleLongPressListener != null)
                    mOnMultipleLongPressListener.onMultipleLongPress();
            }
        };

        setEstimateSizeListener(this);
        addDrawTask(this);
        setTouchEventListener(this);
    }

    private float getRadioValue(String radioAttr) {
        String radio = radioAttr;
        if (radioAttr.lastIndexOf("%") > 0) {
            radio = radio.substring(0, radio.lastIndexOf("%"));
        }

        return Float.parseFloat(radio)/100;
    }


    private void initPosition() {
        // get the center of view to position circle
        mPosX = getEstimatedWidth() / 2;
        mFixedCenterX = mPosX;
        mCenterX = mPosX;
        mPosY = getEstimatedHeight() / 2;
        mFixedCenterY = mPosY;
        mCenterY = mPosY;
        HiLog.info(TAG, " initPosition, mFixedCenterX = mCenterX = "+mFixedCenterX+", mFixedCenterY = mCenterY = "+mFixedCenterY);
    }


    /**
     * Draw the background, the border and the button
     * @param canvas the canvas on which the shapes will be drawn
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        HiLog.info(TAG, "onDraw");

        // Draw the background
        canvas.drawCircle(mFixedCenterX, mFixedCenterY, mBackgroundRadius, mPaintBackground);

        // Draw the circle border
        canvas.drawCircle(mFixedCenterX, mFixedCenterY, mBorderRadius, mPaintCircleBorder);
        HiLog.info(TAG, " mFixedCenterX = "+mFixedCenterX+", mFixedCenterY = "+mFixedCenterY);
        HiLog.info(TAG, " mBackgroundRadius = "+mBackgroundRadius+", mBorderRadius"+mBorderRadius);

        // Draw the button from image
        if (mButtonBitmap != null) {

            PixelMapHolder pixelMapHolder = new PixelMapHolder(mButtonBitmap);
            canvas.drawPixelMapHolder(
                    pixelMapHolder,
                    mPosX + mFixedCenterX - mCenterX - mButtonRadius,
                    mPosY + mFixedCenterY - mCenterY - mButtonRadius,
                    mPaintBitmapButton
            );
        }
        // Draw the button as simple circle
        else {
            canvas.drawCircle(
                    mPosX + mFixedCenterX - mCenterX,
                    mPosY + mFixedCenterY - mCenterY,
                    mButtonRadius,
                    mPaintCircleButton
            );
        }
    }

    private int estWidthSize = -1;
    private int estHeightSize = -1;

    public boolean isSizeChange(int width, int height) {
        boolean result;
        if (estWidthSize == width && estHeightSize == height) {
            result = false;
        } else {
            result = true;
        }
        estWidthSize = width;
        estHeightSize = height;
        return result;
    }

    private void onSizeChanged(int w, int h, int oldW, int oldH) {
        initPosition();

        // radius based on smallest size : height OR width
        int d = Math.min(w, h);
        mButtonRadius = (int) (d / 2f * mButtonSizeRatio);
        mBorderRadius = (int) (d / 2f * mBackgroundSizeRatio);
        mBackgroundRadius = mBorderRadius - (mPaintCircleBorder.getStrokeWidth() / 2f);
        HiLog.info(TAG, "onSizeChanged, mButtonRadius = "+mButtonRadius+", mBorderRadius"+mBorderRadius+", mBackgroundRadius"+mBackgroundRadius);
        if (mButtonBitmap != null) {
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(mButtonRadius * 2, mButtonRadius * 2);
            mButtonBitmap = PixelMap.create(mButtonBitmap, options);
        }
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int d = Math.min(measure(widthMeasureSpec), measure(heightMeasureSpec));

        setEstimatedSize(Component.EstimateSpec.getChildSizeWithMode(d, d, Component.EstimateSpec.PRECISE),
                Component.EstimateSpec.getChildSizeWithMode(d, d, Component.EstimateSpec.PRECISE));

        if (isSizeChange(d ,d)) {
            onSizeChanged(d ,d, estWidthSize, estHeightSize);
        }

        return true;
    }

    private int measure(int measureSpec) {
        if (EstimateSpec.getMode(measureSpec) == EstimateSpec.UNCONSTRAINT) {
            // if no bounds are specified return a default size (200)
            return DEFAULT_SIZE;
        } else {
            // As you want to fill the available space
            // always return the full available bounds.
            return EstimateSpec.getSize(measureSpec);
        }
    }


    /**
     * Handle touch screen motion event. Move the button according to the
     * finger coordinate and detect longPress by multiple pointers only.
     *
     * @param touchEvent The motion event.
     * @return True if the event was handled, false otherwise.
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {

        // if disabled we don't move the
        if (!mEnabled) {
            return true;
        }

        float x = touchEvent.getPointerPosition(touchEvent.getPointerCount()-1).getX();
        float y = touchEvent.getPointerPosition(touchEvent.getPointerCount()-1).getY();
        // to move the button according to the finger coordinate
        // (or limited to one axe according to direction option
        mPosY = mButtonDirection < 0 ? mCenterY : (int) y; // direction negative is horizontal axe
        mPosX = mButtonDirection > 0 ? mCenterX : (int) x; // direction positive is vertical axe
        //HiLog.info(TAG, " mPosY = "+mPosY+", mPosX = "+mPosX);
        HiLog.info(TAG, " touch component = "+component);

        int action = touchEvent.getAction();
        if (action == TouchEvent.PRIMARY_POINT_UP || action == TouchEvent.OTHER_POINT_UP) {
            if (action == TouchEvent.PRIMARY_POINT_UP) {
                HiLog.info(TAG, " onTouchEvent PRIMARY_POINT_UP, PointerCount = " +touchEvent.getPointerCount());
            } else {
                HiLog.info(TAG, " onTouchEvent OTHER_POINT_UP, PointerCount = " +touchEvent.getPointerCount());
            }

            // stop listener because the finger left the touch screen
            mThread.interrupt();

            // re-center the button or not (depending on settings)
            if (mAutoReCenterButton) {
                resetButtonPosition();

                // update now the last strength and angle which should be zero after resetButton
                if (mCallback != null)
                    mCallback.onMove(getAngle(), getStrength());
            }

            if (touchEvent.getPointerCount() == 2){
                // when the last multiple touch is released
                mHandlerMultipleLongPress.removeTask(mRunnableMultipleLongPress);
            }
        }

        if (action == TouchEvent.PRIMARY_POINT_DOWN || action == TouchEvent.OTHER_POINT_DOWN) {
            if (action == TouchEvent.PRIMARY_POINT_DOWN) {
                HiLog.info(TAG, " onTouchEvent PRIMARY_POINT_DOWN, PointerCount = " +touchEvent.getPointerCount());
            } else {
                HiLog.info(TAG, " onTouchEvent OTHER_POINT_DOWN, PointerCount = " +touchEvent.getPointerCount());
            }

            HiLog.info(TAG, " mPosY = "+mPosY+", mPosX = "+mPosX);


            if (mThread != null && mThread.isAlive()) {
                mThread.interrupt();
            }

            mThread = new Thread(this);
            mThread.start();

            if (mCallback != null) {
                mCallback.onMove(getAngle(), getStrength());
            }

            // when the first touch occurs we update the center (if set to auto-defined center)
            if (!mFixedCenter) {
                mCenterX = mPosX;
                mCenterY = mPosY;
            }

            if (touchEvent.getPointerCount() == 2) {
                mHandlerMultipleLongPress.postTask(mRunnableMultipleLongPress, 800);
                mMoveTolerance = MOVE_TOLERANCE;
            }

        }

        if (action == TouchEvent.POINT_MOVE) {
            mMoveTolerance--;
            if (mMoveTolerance == 0) {
                mHandlerMultipleLongPress.removeTask(mRunnableMultipleLongPress);
            }
        }

        double abs = Math.sqrt((mPosX - mCenterX) * (mPosX - mCenterX)
                + (mPosY - mCenterY) * (mPosY - mCenterY));

        // (abs > mBorderRadius) means button is too far therefore we limit to border
        // (buttonStickBorder && abs != 0) means wherever is the button we stick it to the border except when abs == 0
        if (abs > mBorderRadius || (mButtonStickToBorder && abs != 0)) {
            mPosX = (int) ((mPosX - mCenterX) * mBorderRadius / abs + mCenterX);
            mPosY = (int) ((mPosY - mCenterY) * mBorderRadius / abs + mCenterY);
        }

        if (!mAutoReCenterButton) {
            // Now update the last strength and angle if not reset to center
            if (mCallback != null)
                mCallback.onMove(getAngle(), getStrength());
        }

        // to force a new draw
        invalidate();

        return true;
    }


    /*
    GETTERS
     */


    /**
     * Process the angle following the 360° counter-clock protractor rules.
     * @return the angle of the button
     */
    private int getAngle() {
        int angle = (int) Math.toDegrees(Math.atan2(mCenterY - mPosY, mPosX - mCenterX));
        return angle < 0 ? angle + 360 : angle; // make it as a regular counter-clock protractor
    }


    /**
     * Process the strength as a percentage of the distance between the center and the border.
     * @return the strength of the button
     */
    private int getStrength() {
        return (int) (100 * Math.sqrt((mPosX - mCenterX)
                * (mPosX - mCenterX) + (mPosY - mCenterY)
                * (mPosY - mCenterY)) / mBorderRadius);
    }

    /**
     * Reset the button position to the center.
     */
    public void resetButtonPosition() {
        mPosX = mCenterX;
        mPosY = mCenterY;
    }


    /**
     * Return the current direction allowed for the button to move
     * @return Actually return an integer corresponding to the direction:
     * - A negative value is horizontal axe,
     * - A positive value is vertical axe,
     * - Zero means both axes
     */
    public int getButtonDirection() {
        return mButtonDirection;
    }

    /**
     * Return the state of the joystick. False when the button don't move.
     * @return the state of the joystick
     */
    public boolean isEnabled() {
        return mEnabled;
    }


    /**
     * Return the size of the button (as a ratio of the total width/height)
     * Default is 0.25 (25%).
     * @return button size (value between 0.0 and 1.0)
     */
    public float getButtonSizeRatio() {
        return mButtonSizeRatio;
    }


    /**
     * Return the size of the background (as a ratio of the total width/height)
     * Default is 0.75 (75%).
     * @return background size (value between 0.0 and 1.0)
     */
    public float getmBackgroundSizeRatio() {
        return mBackgroundSizeRatio;
    }


    /**
     * Return the current behavior of the auto re-center button
     * @return True if automatically re-centered or False if not
     */
    public boolean isAutoReCenterButton() {
        return mAutoReCenterButton;
    }


    /**
     * Return the current behavior of the button stick to border
     * @return True if the button stick to the border otherwise False
     */
    public boolean isButtonStickToBorder() {
        return mButtonStickToBorder;
    }


    /**
     * Return the relative X coordinate of button center related
     * to top-left virtual corner of the border
     * @return coordinate of X (normalized between 0 and 100)
     */
    public int getNormalizedX() {
        if (getWidth() == 0) {
            return 50;
        }
        return Math.round((mPosX-mButtonRadius)*100.0f/(getWidth()-mButtonRadius*2));
    }

    /**
     * Return the relative Y coordinate of the button center related
     * to top-left virtual corner of the border
     * @return coordinate of Y (normalized between 0 and 100)
     */
    public int getNormalizedY() {
        if (getHeight() == 0) {
            return 50;
        }
        return Math.round((mPosY-mButtonRadius)*100.0f/(getHeight()-mButtonRadius*2));
    }


    /**
     * Return the alpha of the border
     * @return it should be an integer between 0 and 255 previously set
     */
    public int getBorderAlpha() {
        return mBorderAlpha;
    }

     /*
    SETTERS
     */


    /**
     * Set an image to the button with a element
     * @param d PixelMap element to pick the image
     */
    public void setButtonDrawable(Element d) {
        if (d != null) {
            if (d instanceof PixelMapElement) {
                mButtonBitmap = ((PixelMapElement) d).getPixelMap();

                if (mButtonRadius != 0) {
                    PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
                    options.size = new Size(mButtonRadius * 2, mButtonRadius * 2);
                    mButtonBitmap = PixelMap.create(mButtonBitmap, options);
                }

                if (mPaintBitmapButton != null) {
                    mPaintBitmapButton = new Paint();
                }
            }
        }

    }

    /**
     * Set the button color for this JoystickView.
     * @param color the color of the button
     */
    public void setButtonColor(int color) {
        mPaintCircleButton.setColor(new Color(color));
        invalidate();
    }


    /**
     * Set the border color for this JoystickView.
     * @param color the color of the border
     */
    public void setBorderColor(int color) {
        mPaintCircleBorder.setColor(new Color(color));
        if (color != Color.TRANSPARENT.getValue()) {
            mPaintCircleBorder.setAlpha(mBorderAlpha);
        }
        invalidate();
    }

    /**
     * Set the border alpha for this JoystickView.
     * @param alpha the transparency of the border between 0 and 255
     */
    public void setBorderAlpha(int alpha) {
        mBorderAlpha = alpha;
        mPaintCircleBorder.setAlpha(alpha);
        invalidate();
    }

    /**
     * Set the background color for this JoystickView.
     * @param color the color of the background
     */

    public void setBackgroundColor(int color) {
        mPaintBackground.setColor(new Color(color));
        invalidate();
    }

    /**
     * Set the border width for this JoystickView.
     * @param width the width of the border
     */
    public void setBorderWidth(int width) {
        mPaintCircleBorder.setStrokeWidth(width);
        mBackgroundRadius = mBorderRadius - (width / 2.0f);
        invalidate();
    }


    /**
     * Register a callback to be invoked when this JoystickView's button is moved
     * @param l The callback that will run
     */
    public void setOnMoveListener(OnMoveListener l) {
        setOnMoveListener(l, DEFAULT_LOOP_INTERVAL);
    }

    /**
     * Register a callback to be invoked when this JoystickView's button is moved
     * @param l The callback that will run
     * @param loopInterval Refresh rate to be invoked in milliseconds
     */
    public void setOnMoveListener(OnMoveListener l, int loopInterval) {
        mCallback = l;
        mLoopInterval = loopInterval;
    }


    /**
     * Register a callback to be invoked when this JoystickView is touch and held by multiple pointers
     * @param l The callback that will run
     */
    public void setOnMultiLongPressListener(OnMultipleLongPressListener l) {
        mOnMultipleLongPressListener = l;
    }

    /**
     * Set the joystick center's behavior (fixed or auto-defined)
     * @param fixedCenter True for fixed center, False for auto-defined center based on touch down
     */
    public void setFixedCenter(boolean fixedCenter) {
        // if we set to "fixed" we make sure to re-init position related to the width of the joystick
        if (fixedCenter) {
            initPosition();
        }
        mFixedCenter = fixedCenter;
        invalidate();
    }


    /**
     * Enable or disable the joystick
     * @param enabled False mean the button won't move and onMove won't be called
     */
    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
    }

    /**
     * Set the joystick button size (as a fraction of the real width/height)
     * By default it is 25% (0.25).
     * @param newRatio between 0.0 and 1.0
     */
    public void setButtonSizeRatio(float newRatio) {
        if (newRatio > 0.0f & newRatio <= 1.0f) {
            mButtonSizeRatio = newRatio;
        }
    }


    /**
     * Set the joystick button size (as a fraction of the real width/height)
     * By default it is 75% (0.75).
     * Not working if the background is an image.
     * @param newRatio between 0.0 and 1.0
     */
    public void setBackgroundSizeRatio(float newRatio) {
        if (newRatio > 0.0f & newRatio <= 1.0f) {
            mBackgroundSizeRatio = newRatio;
        }
    }

    /**
     * Set the current behavior of the auto re-center button
     * @param b True if automatically re-centered or False if not
     */
    public void setAutoReCenterButton(boolean b) {
        mAutoReCenterButton = b;
    }


    /**
     * Set the current behavior of the button stick to border
     * @param b True if the button stick to the border or False (default) if not
     */
    public void setButtonStickToBorder(boolean b) {
        mButtonStickToBorder = b;
    }


    /**
     * Set the current authorized direction for the button to move
     * @param direction the value will define the authorized direction:
     *                  - any negative value (such as -1) for horizontal axe
     *                  - any positive value (such as 1) for vertical axe
     *                  - zero (0) for the full direction (both axes)
     */
    public void setButtonDirection(int direction) {
        mButtonDirection = direction;
    }


    /*
    IMPLEMENTS
     */

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            //HiLog.info(TAG, "chenyu, while run");
            mContext.getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    if (mCallback != null)
                        mCallback.onMove(getAngle(), getStrength());
                }
            });

            try {
                Thread.sleep(mLoopInterval);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

}
