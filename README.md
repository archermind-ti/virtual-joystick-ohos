# virtual-joystick-ohos

#### 项目介绍
该库提供了一个非常简单且随时可用的自定义视图，可模拟操纵杆

####  安装教程

方式一：

1. 下载virtualjoystick模块

2. 在需要使用的模块中关联使用

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       implementation project(':virtualjoystick')
   	……
   }
   ```

3. gradle sync

方式二：

```
allprojects{
    repositories{
        mavenCentral()
    }
}

implementation 'com.gitee.archermind-ti:virtualjoystick:1.0.0-beta'
```

#### 使用说明

一个非常简单的片段展示如何使用， 只需设置 onMoveListener 即可检索其角度和强度

```
@Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        
        ...

        JoystickView joystick = (JoystickView) findComponentById(ResourceTable.Id_joystickView);
        joystickLeft.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                // do whatever you want
            }
        });
    }
```

角度遵循简单的反时钟量角器的规则。 强度是按钮从中心到边框的距离的百分比。

<img src="/misc/virtual-joystick.png" style="zoom: 33%;" />



默认情况下，获取数据的刷新率为 20/秒（每 50 毫秒）。 如果您想要更多或更少，只需使用更多参数设置侦听器以设置以毫秒为单位的刷新率。

```
// around 60/sec
joystick.setOnMoveListener(new JoystickView.OnMoveListener() { ... }, 17); 
```

#### 支持的属性

您可以根据这些属性自定义摇杆：

 JV_buttonImage、JV_buttonColor、JV_buttonSizeRatio、JV_borderColor、JV_borderAlpha、JV_borderWidth、JV_backgroundColor、JV_backgroundSizeRatio、JV_fixedCenter、JV_autoReCenterButton、JV_buttonStickToBorder、JV_Direction 和 JV_button

例如：

```
<io.github.controlwear.virtual.joystick.JoystickView
        xmlns:custom="http://schemas.huawei.com/res/ohos-auto"
        ohos:id="$+id:joystickView_right"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_bottom="true"
        ohos:align_parent_right="true"
        ohos:top_margin="64vp"
        custom:JV_borderWidth="8vp"
        custom:JV_backgroundColor="#009688"
        custom:JV_borderColor="#00796B"
        custom:JV_buttonColor="#FF6E40"
        />
```

##### Image

如果想要多更自定义操纵杆，可以使用 JV_buttonImage 和背景属性来指定， 图像将自动调整大小。

如果指定了 JV_buttonImage，就不需要 JV_buttonColor

```
<io.github.controlwear.virtual.joystick.JoystickView
        xmlns:custom="http://schemas.huawei.com/res/ohos-auto"
        ohos:id="$+id:joystickView_left"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_bottom="true"
        ohos:margin="32vp"
        ohos:background_element="$media:joystick_background"
        custom:JV_buttonImage="$media:pink_ball"
        custom:JV_fixedCenter="false"/>
```

##### SizeRatio

我们可以更改按钮和背景的默认大小，大小计算为总宽度/高度的百分比。默认情况下，按钮为 25% (0.25)，背景为 75% (0.25)。

如果总数（背景 + 按钮）在 1.0 以上，按钮在边框上可能会被剪掉一点。

```
<...
    custom:JV_buttonSizeRatio="50%"
    custom:JV_backgroundSizeRatio="10%"/>
```

```
joystick.setBackgroundSizeRatio(0.5);
joystick.setButtonSizeRatio(0.1);
```

背景大小不适用于自定义图片。

##### FixedCenter or Not? (and auto re-center)

如果不设置该参数，则默认为FixedCenter，这是常规行为。

然而，有时有一个自动定义的中心会很方便，每次你用手指触摸屏幕时都会定义它（中心位置将被限制在 JoystickView 的宽度/高度内），您可以在 xml（如上）或 Java 中设置：

```
joystick.setFixedCenter(false); // set up auto-define center
```

当用户无法（或不想）看到屏幕（例如无人机的控制器）时，UnfixedCenter（设置为 false）特别方便。

我们也可以删除自动重新居中的按钮，只需将其设置为 false。

```
joystick.setAutoReCenterButton(false);
```

（如果我们同时设置删除 FixedCenter 和 AutoReCenter，行为会有点奇怪。）

##### Enabled

默认情况下，操纵杆处于启用状态（设置为 True），但您可以在 xml 或 Java 中禁用它。 然后，按钮将停止移动，并且不会再调用 onMove()。

```
joystick.setEnabled(false); // disabled the joystick
joystick.isEnabled(); // return enabled state
```

##### ButtonDirection

默认情况下，按钮可以在 X、Y 两个方向上移动（常规行为），但我们可以通过一个水平或垂直轴限制移动。

```
<...
    custom:JV_buttonDirection="horizontal"/>
```

在布局文件 (xml) 中，此选项可以设置为horizontal, vertical或者 both

我们还可以通过设置整数值在 Java 文件中设置此选项：

- horizontal： 任何负值（如-1）
- vertical： 任何正值（如1）
- both： 0

```
joystick.setButtonDirection(1); // vertical
```

##### Wearable

在可穿戴应用程序中使用这个库，操纵杆模式可能会禁用滑动关闭手势并实现长按关闭模式，我们还设置了另一个侦听器：OnMultipleLongPressListener，它将用于使用多个指针（至少两个手指）而不是一个指针来调用。

```
joystick.setOnMultiLongPressListener(new JoystickView.OnMultipleLongPressListener() {
    @Override
    public void onMultipleLongPress() {
        ... // eg. mDismissOverlay.show();
    }
});
```

#### Demo

<img src="/misc/joystick-demo.gif" style="zoom: 25%;" />



#### 版权和许可信息

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

#### 版本迭代

V1.0.0
