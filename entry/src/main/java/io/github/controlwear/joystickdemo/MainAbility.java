package io.github.controlwear.joystickdemo;

import io.github.controlwear.joystickdemo.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;

public class MainAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        if (verifySelfPermission("ohos.permission.MULTIMODAL_INTERACTIVE") != IBundleManager.PERMISSION_GRANTED) {
            requestPermissionsFromUser(
                    new String[] { "ohos.permission.MULTIMODAL_INTERACTIVE" } , 0);
        }

        super.setMainRoute(MainAbilitySlice.class.getName());
        
    }
}
