/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.github.controlwear.joystickdemo.slice;

import io.github.controlwear.joystickdemo.ResourceTable;
import io.github.controlwear.virtual.joystick.JoystickView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class MainAbilitySlice extends AbilitySlice {
    private Text mTextAngleLeft;
    private Text mTextStrengthLeft;

    private Text mTextAngleRight;
    private Text mTextStrengthRight;
    private Text mTextCoordinateRight;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mTextAngleLeft = (Text) findComponentById(ResourceTable.Id_text_angle_left);
        mTextStrengthLeft = (Text) findComponentById(ResourceTable.Id_text_strength_left);

        JoystickView joystickLeft = (JoystickView) findComponentById(ResourceTable.Id_joystickView_left);
        joystickLeft.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                mTextAngleLeft.setText(angle + "°");
                mTextStrengthLeft.setText(strength + "%");
            }
        });


        mTextAngleRight = (Text) findComponentById(ResourceTable.Id_text_angle_right);
        mTextStrengthRight = (Text) findComponentById(ResourceTable.Id_text_strength_right);
        mTextCoordinateRight = (Text) findComponentById(ResourceTable.Id_text_coordinate_right);

        JoystickView joystickRight = (JoystickView) findComponentById(ResourceTable.Id_joystickView_right);
        joystickRight.setOnMoveListener(new JoystickView.OnMoveListener() {

            @Override
            public void onMove(int angle, int strength) {
                mTextAngleRight.setText(angle + "°");
                mTextStrengthRight.setText(strength + "%");
                mTextCoordinateRight.setText(
                        String.format("x%03d:y%03d",
                                joystickRight.getNormalizedX(),
                                joystickRight.getNormalizedY())
                );
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
